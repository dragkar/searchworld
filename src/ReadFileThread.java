import java.io.*;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 * Created by admin on 11.12.2017.
 */
public class ReadFileThread extends Thread implements ReadFileInterface {

    Lock lockWrite = new ReentrantLock();
    Charset charset = StandardCharsets.UTF_8;
    private String fullFileName;

    public ReadFileThread(String fullFileName) {
        this.fullFileName = fullFileName;
        Const.wordSearch = Const.wordSearch.toLowerCase();
    }

    @Override
    public void getFileContent() {

        String string;

        try {
            BufferedReader in = new BufferedReader(new FileReader(fullFileName));
            int count = 0;
            while ((string = in.readLine()) != null) {

                count = searchWord(string, count);
                if (count >= 5) {
                    Const.count += count;
                    count = 0;
                    writeCount(Const.count);
                }
            }
            Const.count += count;
            if (count > 0)
                writeCount(Const.count);

            in.close();

        } catch (FileNotFoundException e) {
            e.printStackTrace();

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void run() {
        getFileContent();

    }

    public int searchWord(String message, int count) {
        //      System.out.println(Const.wordSearch.toLowerCase());
        message = message.toLowerCase();

        int i = message.indexOf(Const.wordSearch);
        while (i >= 0) {
            count++;
            i = message.indexOf(Const.wordSearch, i + 1);
        }
        return count;
    }

    private void writeCount(int count) {
        lockWrite.lock();
        try (final FileWriter writer = new FileWriter("count.txt", true)) {
            final String s = Integer.toString(count);
            writer.append(s);
            writer.write(System.lineSeparator());
            lockWrite.unlock();
        } catch (IOException e) {
            System.out.println(e.getMessage());
        }

    }
}
