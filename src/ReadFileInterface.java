/**
 * Created by admin on 11.12.2017.
 */
public interface ReadFileInterface {

    void getFileContent();

    int searchWord(String message, int count);

}
